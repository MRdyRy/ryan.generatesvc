package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "master_tiering_product")
public class MasterTieringProduct extends AuditTrail{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MASTER_TIERING_PRODUCT_ID")
    private int masterTieringProductId;

    @Column(name = "CURRENCY", nullable = false, length = 3)
    private String currency;

    @Column(name = "TIER_SALDO")
    private String tierSaldo;

    @Column(name = "INTEREST_RATE")
    private double interestRate;

    @Column(name = "NISBAH")
    private double nisbah;

}
