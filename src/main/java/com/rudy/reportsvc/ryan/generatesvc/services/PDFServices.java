package com.rudy.reportsvc.ryan.generatesvc.services;

import com.rudy.reportsvc.ryan.generatesvc.dto.ReportRequest;
import com.rudy.reportsvc.ryan.generatesvc.model.MasterTemplate;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;

public interface PDFServices {
    ResponseEntity<byte[]> getTemplateByProduct(ReportRequest reportRequest) throws Exception;
    MasterTemplate getTemplateByProduct(String productCode);
}
