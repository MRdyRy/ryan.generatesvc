package com.rudy.reportsvc.ryan.generatesvc.repository;

import com.rudy.reportsvc.ryan.generatesvc.model.MasterProductTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterProductTemplateRepository extends JpaRepository<MasterProductTemplate,Integer> {
    @Query("SELECT mp FROM MasterProductTemplate mp WHERE mp.productCode = :productCode and mp.currency = :currency")
    MasterProductTemplate findMasterProductTemplateByProductCodeAndCurrency(@Param("productCode") String producCode, @Param("currency") String currency);
}
