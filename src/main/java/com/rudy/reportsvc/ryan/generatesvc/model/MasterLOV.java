package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "master_lov")
public class MasterLOV extends AuditTrail{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "LOV_ID")
    private int lovId;

    @Column(name = "MODULE")
    private String module;

    @Column(name = "CODE")
    private String code;

    @Column(name = "DESC_IN")
    private String descIn;

    @Column(name = "DESC_EN")
    private String descEn;
}
