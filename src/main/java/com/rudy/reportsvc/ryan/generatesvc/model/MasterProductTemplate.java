package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "master_product_template")
public class MasterProductTemplate extends AuditTrail{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MASTER_PRODUCT_ID")
    private int masterProductId;

    @Column(name = "PRODUCT_CODE",nullable = false)
    private String productCode;

    @Column(name = "CURRENCY", nullable = false, length = 3)
    private String currency;

    @Column(name = "TEMPLATE_ID")
    private String templateId;

    @Column(name = "IMAGE_URL")
    private String imageUrl;


}
