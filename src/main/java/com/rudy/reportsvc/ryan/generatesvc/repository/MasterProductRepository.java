package com.rudy.reportsvc.ryan.generatesvc.repository;

import com.rudy.reportsvc.ryan.generatesvc.model.MasterProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterProductRepository extends JpaRepository<MasterProduct, Integer> {
    @Query(value = "SELECT m FROM MASTER_PRODUCT m WHERE m.PRODUCT_CODE = ?1", nativeQuery = true)
    MasterProduct findMasterProductByProductCode(String productCode);
}
