package com.rudy.reportsvc.ryan.generatesvc.controller;

import com.rudy.reportsvc.ryan.generatesvc.dto.ReportRequest;
import com.rudy.reportsvc.ryan.generatesvc.services.impl.PDFServicesImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@Slf4j
@RequestMapping(value = "/api")
@Api(tags = "REPORT SERVICES", value = "report")
public class ReportController {

    @Autowired
    PDFServicesImpl pdfServicesImpl;

    @ApiOperation(value = "Generate PDF by product")
    @CrossOrigin
    @PostMapping(value = "/generate/pdf",  consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<?> generateByType(@RequestBody ReportRequest reportRequest) throws Exception {

        log.info("Start process request report service for product : "+reportRequest);
        return pdfServicesImpl.getTemplateByProduct(reportRequest);


    }

}
