package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "master_template")
public class MasterTemplate extends AuditTrail{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MASTER_TEMPLATE_ID")
    private int master_template_id;

    @Column(name = "CONTENT_TEMPLATE_LIST", nullable = false)
    private String content_template_list;

    @Column(name = "DESCRIPTION_HEADER")
    private String descriptionHeader;
    
}
