package com.rudy.reportsvc.ryan.generatesvc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportRequest extends TransactionRequest{
    private String productCode;
}
