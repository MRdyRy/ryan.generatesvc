package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "master_content_template")
public class MasterContentTemplate extends AuditTrail{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int contentTemplateId;
    @Column(name = "DESCRIPTION", nullable = false)
    private String desc;
}
