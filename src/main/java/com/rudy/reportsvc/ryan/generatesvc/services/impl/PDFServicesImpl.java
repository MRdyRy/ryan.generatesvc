package com.rudy.reportsvc.ryan.generatesvc.services.impl;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.rudy.reportsvc.ryan.generatesvc.dto.ReportRequest;
import com.rudy.reportsvc.ryan.generatesvc.exception.ReportServiceException;
import com.rudy.reportsvc.ryan.generatesvc.model.MasterProduct;
import com.rudy.reportsvc.ryan.generatesvc.model.MasterTemplate;
import com.rudy.reportsvc.ryan.generatesvc.repository.MasterProductRepository;
import com.rudy.reportsvc.ryan.generatesvc.services.PDFServices;
import com.rudy.reportsvc.ryan.generatesvc.utils.ReportUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class PDFServicesImpl implements PDFServices {

    @Autowired
    MasterProductRepository masterProductRepository;

    @Autowired
    TemplateEngine templateEngine;

    @Autowired
    ResourceLoader resourceLoader;

    @Value("${BASE_URL}")
    private String BASE_ASSET_URL;

    @Override
    public ResponseEntity<byte[]> getTemplateByProduct(ReportRequest reportRequest) throws Exception {
        log.info("PROCESS GENERATE REPORT : "+reportRequest);
        Optional<MasterProduct> product = masterProductRepository.findById(Integer.parseInt(reportRequest.getProductCode()));
        log.info("GET DATA PRODUCT : "+reportRequest);
        if(product.isPresent()){
            log.info("PRODUCT FIND : "+product.get());
            String outputName = ReportUtils.generateNameFile(reportRequest.getProductCode(),new Date());
            log.info("PROCESS GENERATE REPORT :::");
            return generateReport(product.get(), reportRequest,"RiplayForm", outputName);
        }else{
            throw new ReportServiceException("PRODUCT NOT FOUND");
        }
    }

    private ResponseEntity<byte[]> generateReport(MasterProduct data, ReportRequest reportRequest, String templateName, String outputNameFile) {
        log.info("GENERATE REPORT PDF {} Data " + data + " | " + outputNameFile);
        Object obj =null;
        try {

            // passing parameter
            Context context = new Context();
            context.setVariable("data", data);
            context.setVariable("req",reportRequest);

            String processedHtml = templateEngine.process(templateName, context);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();

            ConverterProperties converterProperties = new ConverterProperties();
            converterProperties.setBaseUri(BASE_ASSET_URL);

            HtmlConverter.convertToPdf(processedHtml, stream, converterProperties);

            byte[] bytes = stream.toByteArray();
            stream.flush();

            log.info("outputFileName {}", outputNameFile);

            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+outputNameFile)
                    .body(bytes);
        } catch (Exception e) {
            log.error("Error when generating PDF");
            log.error("Error {}", e);
        }
        return null;
    }

    @Override
    public MasterTemplate getTemplateByProduct(String productCode) {
        return null;
    }
}
