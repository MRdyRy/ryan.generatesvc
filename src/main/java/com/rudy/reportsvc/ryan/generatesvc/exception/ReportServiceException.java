package com.rudy.reportsvc.ryan.generatesvc.exception;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class ReportServiceException extends RuntimeException{
    private final String message;

    public ReportServiceException(String message) {
        this.message = message;
    }
}
