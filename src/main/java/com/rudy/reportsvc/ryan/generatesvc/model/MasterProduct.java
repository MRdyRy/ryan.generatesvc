package com.rudy.reportsvc.ryan.generatesvc.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Table(name = "master_product")
public class MasterProduct extends AuditTrail implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "MASTER_PRODUCT_ID")
    private int masterProductId;

    @Column(name = "PRODUCT_CODE", nullable = false)
    private String productCode;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @Column(name = "FEATURE_SERVICE")
    private String featureService;

    @Column(name = "RISK")
    private String risk;

    @Column(name = "PERSYARATAN_TATA_CARA")
    private String persyaratanTataCara;
}
