package com.rudy.reportsvc.ryan.generatesvc.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReportUtils {
    public static SimpleDateFormat sdfDateOnly = new SimpleDateFormat("dd-MM-yyyy");
    public static SimpleDateFormat sdfDateTime = new SimpleDateFormat("dd-MM-yyyy HH_mm_ss");

    public static String generateNameFile(String productCode, Date date){
       StringBuilder sb = new StringBuilder();
        if(null!=productCode||"".equalsIgnoreCase(productCode))
           sb.append(productCode);
        sb.append("_").append(sdfDateTime.format(date)).append(".pdf");
        return sb.toString();
    }

    public static byte[] serializeObject(Object obj) throws IOException
    {
        ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bytesOut);
        oos.writeObject(obj);
        oos.flush();
        byte[] bytes = bytesOut.toByteArray();
        bytesOut.close();
        oos.close();
        return bytes;
    }
    
}
